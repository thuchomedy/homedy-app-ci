import React, { PureComponent } from 'react';
import moment from 'moment';
import androidLogo from './android.svg';
import iosLogo from './iOS.png'
import './App.css';

const logo = "https://static.homedy.com/src/images/logo-homedy-295x74.png";
class App extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      version: null,
      buildNumber: 0,
      buildAt: null,
      isIOS: false
    }
  }

  componentDidMount() {
    document.title = "Anh hàng xóm";
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        this.setState({isIOS: true})
        this.getUrl(true);
    } else this.getUrl(false);
    
  }

  async getUrl(isIOS) {
    const result = await fetch('https://s3-ap-southeast-1.amazonaws.com/homedy.bigdata/app.json', {crossDomain:true}).then(response => response.json());
    this.setState({version: result.version, buildNumber: result.builds, buildAt: moment(result.timestamp, 'x').format('hh:mm DD/MM/YYYY')});
    let url;
    if (isIOS) url = `itms-services://?action=download-manifest&url=https://s3-ap-southeast-1.amazonaws.com/homedy.bigdata/homedyapp/beta/ios/v${result.version}_b${result.builds}/HomedyApp.plist`;
    else url = `https://s3-ap-southeast-1.amazonaws.com/homedy.bigdata/homedyapp/beta/android/${result.builds}_${result.version}/app-universal-release.apk`;
    this.setState({url});
  }

  render() {
    const { version, buildNumber, buildAt, isIOS, url } = this.state;

    return (
      <div style={styles.container}>
        <div style={styles.section}>
          <img src={logo} style={{marginTop: 50, marginBottom: 80}} alt="logo" />
          <h1 style={styles.title}>Homedy App Beta</h1>
          <span style={styles.version}>{version} ({buildNumber})</span>
          <a className="shadow-button" href={url} style={{ backgroundColor: isIOS ? 'rgb(48, 87, 104)' : 'rgb(40, 213, 122)', marginBottom: 10, ...styles.button }}>
            <img src={ isIOS ? iosLogo : androidLogo } style={{ padding: isIOS ? 0 : 5, ...styles.icon }} alt="logo" />
            <span style={{ color: isIOS ? 'rgb(219, 219, 219)' : 'white', ...styles.buttonText }}>
              Download
            </span>
          </a>
          <i>Build at: {buildAt}</i>
        </div>
        <span style={styles.tos}>
          Copyright © 2019 Homedy Inc. All rights reserved.
        </span>
      </div>
    );
  }
}

const styles = {
  container: {
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
    paddingLeft: 20,
    paddingRight: 20,
    height: '100vh'
  },
  section: {alignItems: 'center', display: 'flex', flexDirection: 'column'},
  title: { color: '#01729b', marginBottom: 10 },
  version: { color: 'black', fontWeight: 'bold', marginBottom: 30 },
  buildInfo: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginBottom: 10
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 5,
    textDecoration: 'none',
    borderWidth: 1,
    borderColor: '#9b9b9b',
    borderStyle: 'solid',
    display: 'flex',
    width: '100%',
    boxSizing: 'border-box'
  },
  icon: {
    width: 52,
    height: 50,
    marginRight: 10,
    backgroundColor: 'white',
    borderRadius: 5
  },
  buttonText: { fontSize: 50, fontWeight: 'bold' },
  tos: { marginBottom: 10, fontSize: 14 }
}

export default App;
